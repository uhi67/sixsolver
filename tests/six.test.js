/**
 * Testing six.js
 *
 * This script runs only in nodejs environment.
 */
const six = require('../lib/six.js');
const fns = six.fns;
const Expression = six.Expression;
const Fn = six.Fn;
const State = six.State;
const Problem = six.Problem;
const StateSet = six.StateSet;
const Database = six.Database;

const format = require('string-format');
format.extend(String.prototype, {});

expect.extend({
	m(validator, message) {
		console.log(this);
		return {
			message: () => `"${message}"`,
			pass: true
		};
	}
});

test('Static', () => {
	expect(Fn.fakt(4)).toBe(24);
	expect(Fn.variable('$x').toString()).toBe('<mi>$x</mi>');
	expect(fns.get('+').toString([1, 2])).toBe('1<mo>+</mo>2');
});

test('Fn', () => {
	expect(Fn.variable('kalap').toString()).toBe('<mi>kalap</mi>');
	expect(Fn.variable('kalap').evaluate({kalap: 13})).toBe(13);

	let fn = fns.get('*');
	expect(fn.evaluate([3,4])).toBe(12);
});

test('Expression', () => {
	let exp = Expression.variable('$b');
	expect(exp.mathml()).toBe('<mi>$b</mi>');
	expect(exp.evaluate({$b:23})).toBe(23);

	let exp2 = new Expression(fns.get('+'), [Expression.variable('$a'), Expression.variable('$b')]);
	expect(exp2.mathml()).toBe('<mi>$a</mi><mo>+</mo><mi>$b</mi>');
	expect(exp2.evaluate({$a:2, $b:3})).toBe(5);

	let exp2a = new Expression(fns.get('+'), [Expression.variable('$a'), Expression.variable('$b')]);
	expect(exp2.isSame(exp2a)).toBe(true);
	expect(exp.isSame(exp2a)).toBe(false);
	let exp2b = new Expression(fns.get('+'), [Expression.variable('$a'), Expression.variable('$c')]);
	expect(exp2.isSame(exp2b)).toBe(false);

	let exp3 = new Expression(fns.get('*'), [exp2, exp2b]); // (a + b) * (a + c)
	expect(exp3.evaluate({$a:2, $b:3, $c:4})).toBe((2+3)*(2+4));
	expect(exp3.mathml()).toBe('<mrow><mo>(</mo><mi>$a</mi><mo>+</mo><mi>$b</mi><mo>)</mo></mrow><mo>*</mo><mrow><mo>(</mo><mi>$a</mi><mo>+</mo><mi>$c</mi><mo>)</mo></mrow>');

	let expx = Expression.create(fns, ['+', '$a', 2]);
	expect(expx.mathml()).toBe('<mi>$a</mi><mo>+</mo><mn>2</mn>');
	expect(expx.evaluate({'$a': 5})).toBe(7);

});

test('State', () => {
	let expa = Expression.variable('a');
	let expb = Expression.variable('b');
	let expc = Expression.variable('c');
	let exp1 = new Expression(fns.get('+'), [expa, expb]); // a + b
	let exp2 = new Expression(fns.get('*'), [expb, expc]); // b * c
	let exp3 = new Expression(fns.get('-'), [exp1, exp2]); // (a + b) - b * c

	let state1 = new State([exp3], null); // (a + b) - b * c
	let state2 = new State([exp1, exp2], null);

	expect(state1.isSame(state2)).toBe(false);
	expect(state1.isSame(new State([exp1, exp2], null))).toBe(false);
	expect(state2.isSame(new State([
		new Expression(fns.get('+'), [expa, expb]), // (a+b)
		new Expression(fns.get('*'), [expb, expc])  // (b*c)
	], null))).toBe(true);

	problem1 = new Problem({a:4, b:3, c:2, d:1}, 1);
	problem2 = new Problem({a:4, b:3, c:2, d:1}, 6);
	expect(state2.isSolutionOf(problem1)).toBe(false);
	expect(state1.explist[0].evaluate(problem1.bindings)).toBe(problem1.result); // (a + b) - b * c

	expect(Array.isArray(state1.explist)).toBe(true);
	expect(state1.explist.length > 1).toBe(false);
	expect(state1.explist[0].evaluate(problem1.bindings) === problem1.result).toBe(true);

	expect(state1.isSolutionOf(problem1)).toBe(true);
	expect(state1.isSolutionOf(problem2)).toBe(false);
});

test('StateSet', () => {
	let state1 = new State([Expression.create(fns, ['+', '$a', ['*', '$b', '$c']])]);
	let state2 = new State([Expression.create(fns, ['$a']), Expression.create(fns, ['*', '$b', '$c'])]);

	let stateSet = new StateSet();
	stateSet.push(state1);
	expect(stateSet.contains(state1)).toBe(true);
	expect(stateSet.contains(state2)).toBe(false);
	stateSet.remove(state1);
	expect(stateSet.contains(state1)).toBe(false);
});

test('Expressions', () => {
	fns1 = new Map(fns);
	fns1.set(':=', six.fnLet);
	fns1.set('sum', six.fnLet);
	[ // exp, displ, bind, result, [bindresult]
		[['neg', '$a'], '<mo>(</mo><mo>-</mo><mi>$a</mi><mo>)</mo>', {$a:1}, -1],
		[['*', 5, ['neg', '$a']], '<mn>5</mn><mo>*</mo><mrow><mo>(</mo><mo>-</mo><mi>$a</mi><mo>)</mo></mrow>', {$a:1}, -5],
		[['+', '$a', '$b', 3], '<mi>$a</mi><mo>+</mo><mi>$b</mi><mo>+</mo><mn>3</mn>', {$a:1, $b:2}, 6],
		[['/', '$a', 2], '<mfrac><mi>$a</mi><mn>2</mn></mfrac>', {$a:32}, 16],
		[['*', 2, ['+', '$b', 3]], '<mn>2</mn><mo>*</mo><mrow><mo>(</mo><mi>$b</mi><mo>+</mo><mn>3</mn><mo>)</mo></mrow>', {$b:2}, 10],
		[['sqrt', ['/', '$a', 2]], '<msqrt><mfrac><mi>$a</mi><mn>2</mn></mfrac></msqrt>', {$a:32}, 4],
		[['root', 3, ['*', '$a', 5]], '<mroot><mrow><mi>$a</mi><mo>*</mo><mn>5</mn></mrow><mn>3</mn></mroot>', {$a:25}, 5],
		[['/',
			['-', ['neg', ['$b']], ['sqrt', ['-', ['^', '$b', 2], ['*', 4, '$a', '$c']]]],
			['*', 2, ['$a']]
		], `<mfrac>
		<mrow>
			<mrow><mo>(</mo><mo>-</mo><mi>$b</mi><mo>)</mo></mrow>
			<mo>-</mo>
			<msqrt><mrow><mrow><msup><mi>$b</mi><mn><mn>2</mn></mn></msup></mrow><mo>-</mo><mrow><mn>4</mn><mo>*</mo><mi>$a</mi><mo>*</mo><mi>$c</mi></mrow></mrow></msqrt>
		</mrow>
		<mrow>
			<mn>2</mn>
			<mo>*</mo>
			<mi>$a</mi>
		</mrow>
		</mfrac>`, {$a:2, $b:5, $c:2}, -2],
		[[':=', '$a', 7], '<mi>$a</mi><mo>:=</mo><mn>7</mn>', {}, 7, {'$a': 7}],
	].forEach((d, i) => {
		let [exp, displ, bind, result] = d;
		displ = displ.replace(/\s+/g, '');
		let expression = Expression.create(fns1, exp);
		//console.log(i);
		expect(expression.mathml()).toBe(displ);
		expect(expression.evaluate(bind)).toBe(result);
		if(d.length > 4) {
			let bindresult = d[4];
			expect(bind).toEqual(bindresult);
		}
	});
});

test('fnLet', ()=> {
	let fns1 = new Map(fns);
	fns1.set(',', new six.Fn(',', 2, 99, null, v => v));
	fns1.set(':=', six.fnLet);
	fns1.set('=', new six.Fn('=', 2, 9, null, v => v.shift() === v.shift()));

	let exp0 = Expression.create(fns1, [',',
		[':=', '$a', 2],
		[':=', '$b', -5],
		[':=', '$c', 2],
	]);
	expect(exp0.ops[0].mathml()).toBe('<mi>$a</mi><mo>:=</mo><mn>2</mn>');
	expect(exp0.mathml()).toBe('<mrow><mi>$a</mi><mo>:=</mo><mn>2</mn></mrow><mo>,</mo><mrow><mi>$b</mi><mo>:=</mo><mn>-5</mn></mrow><mo>,</mo><mrow><mi>$c</mi><mo>:=</mo><mn>2</mn></mrow>');
	let b = {};
	expect(exp0.evaluate(b)).toEqual([2,-5,2]);
	expect(b).toEqual({$a:2, $b:-5, $c:2});
});

test('fnSum', ()=> {
	let fns1 = new Map(fns);
	fns1.set('Sum', six.fnSum);

	let exp = Expression.create(fns1, ['Sum', 'n', 1, 5, 'n']);
	expect(exp.math()).toBe('<math xmlns="http://www.w3.org/1998/Math/MathML"><munderover><mo>&Sum;</mo><mrow>{$1}<mo>=</mo>{$2}</mrow>{$3}</munderover>{$4}</math>'.format({
		$1: '<mi>n</mi>',
		$2: '<mn>1</mn>',
		$3: '<mn>5</mn>',
		$4: '<mi>n</mi>'
	}));
	let b = {};
	expect(exp.evaluate(b)).toEqual(15);
	expect(b).toEqual({n:5});

	let exp1 = Expression.create(fns1, ['Sum', 'i', 1, 5, ['^', 'i', 'n']]);
	expect(exp1.evaluate({n:2})).toEqual(55);
	expect(exp1.evaluate({n:-2})).toBeCloseTo(1.4636);

});

test('Database', ()=>{
	let fns1 = new Map(['+', '-', '*', '/', 'sqrt'].map(o=>[o,fns.get(o)]));
	problem = new Problem({a:1, b:1}, 2);
	let database = new Database(problem, fns1, 2);
	let startState = new State(Object.keys(problem.bindings).map(v => Expression.create(fns, v)));
	database.extend(startState);
	expect(database.closed.indexOf(startState)).toEqual(0);
	// Van benne olyan állapot
	s1 = new State([Expression.create(fns1,['+', 'a', 'b'])]);
	expect(database.open.findIndex(s => s.isSame(s1))).not.toEqual(-1);
	s2 = new State([Expression.create(fns1,'a'), Expression.create(fns1,['sqrt', 'b'])]);
	expect(database.open.findIndex(s => s.isSame(s2))).not.toEqual(-1);

	problem = new Problem({a:1, b:1}, 2);
	database = new Database(problem, fns1, 3);
	startState = new State(Object.keys(problem.bindings).map(v => Expression.create(fns, v)));
	let result = database.search(startState);
	expect(result).toBe(true);
	expect(problem.solution).toBeInstanceOf(State);
	expect(problem.solution.toString()).toBe('S<[+(a,b)]:1->[a,b]>');

	problem = new Problem({a:1, b:1}, 9);
	database = new Database(problem, fns1, 3);
	startState = new State(Object.keys(problem.bindings).map(v => Expression.create(fns, v)));
	result = database.search(startState);
	expect(problem.solution).toBe(null);
	expect(result).toBe(false);

	problem = new Problem({a:3, b:3, c:3}, 6);
	database = new Database(problem, fns1, 4);
	startState = new State(Object.keys(problem.bindings).map(v => Expression.create(fns, v)));
	result = database.search(startState);
	//console.log(result, problem.solution.toString());
	expect(result).toBe(true);
	expect(problem.solution).toBeInstanceOf(State);
	let solutions = ['S<[+(*(sqrt(a),sqrt(b)),c)]:4->[*(sqrt(a),sqrt(b)),c]>', 'S<[-(*(a,b),c)]:2->[*(a,b),c]>'];
	expect(problem.solution.toString()).toBe('S<[-(*(a,b),c)]:2->[*(a,b),c]>');
	/*
	database.closed.forEach(s => {
		if(s.isSolutionOf(problem)) console.log(s.toString());
	});
	*/

	let fns2 = new Map(['+', '-', '*', '/', 'fakt'].map(o=>[o,fns.get(o)]));
	problem = new Problem({a:1, b:1, c:1}, 6);
	database = new Database(problem, fns2, 4);
	startState = new State(Object.keys(problem.bindings).map(v => Expression.create(fns, v)));
	result = database.search(startState);
	database.closed.forEach(s => {
		if(s.explist.length===1) console.log(s.explist[0].evaluate(problem.bindings), s.toString());
	});
	expect(result).toBe(true);
	expect(problem.solution).toBeInstanceOf(State);
	expect(problem.solution.toString()).toBe('S<[fakt(+(+(a,b),c))]:3->[+(+(a,b),c)]>');


	/*
	console.log(database.closed.map(s => s.explist.length===1 ? s.explist[0].evaluate(problem.bindings) : '_'));
	console.log(result, problem.solution ? problem.solution.toString() : 'nincs');
	*/
});
