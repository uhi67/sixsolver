/**
 * Simple demo of http module
 */
const http = require('http');
const url = require("url");
const querystring = require('querystring');

const markdown = require('markdown').markdown;
console.log(markdown.toHTML('A paragraph in **markdown**!'));

const server = http.createServer((req, res) => {
	let page = url.parse(req.url).pathname;
	let params = querystring.parse(url.parse(req.url).query);
	console.log(page);

	res.writeHead(200, {"Content-Type": "text/html"});
	res.write('<!DOCTYPE html>');
	res.write('<h1>Hello, World!</h1>');
	res.write('<p>Hi everybody!</p>');
	res.end();
});

server.on('close', () => {
	console.log('Goodbye!');
});

server.listen(8080);
