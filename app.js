const path = require('path');
const ejs = require('ejs');
const fs = require('fs');
const express = require('express');
const requestIp = require('request-ip');
const morgan = require('morgan');
//const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');
const errorhandler = require('errorhandler');
const rfs = require('rotating-file-stream');
const compiless = require('express-compiless');
const passport = require('passport');
const favicon = require('serve-favicon'); // loads the piece of middleware for the favicon
const bootstrapPath = path.dirname(require.resolve("bootstrap/package.json"));

const app = express();

// Config
const env = process.env.NODE_ENV || 'development';
const config = require('./config/config')[env];
if(typeof config === 'undefined') console.log('Configuration error: config of `'+env+'` is undefined');
require('./config/passport')(passport, config);

// Log
const accessLogStream = rfs('access.log', {
	interval: '1d', // rotate daily
	path: path.join(__dirname, 'log')
});
app.use(morgan(config.app.logformat, { stream: accessLogStream }));

// Preprocess
app.use(compiless({ root: config.app.publicdir}));
app.use(requestIp.mw());
//app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false, limit: '999kb'}));
app.use(session({
	resave: true,
	saveUninitialized: true,
	secret: config.session.secret,
}));
app.use(passport.initialize());
app.use(passport.session());

// Static resources
app.use(express.static(bootstrapPath+'/dist'));
app.use(express.static(config.app.publicdir));
app.use(favicon(config.app.publicdir + '/favicon.ico'));

// Routes (applied in definition order)
require('./routes/app')(app);
require('./routes/index')(app);
require('./routes/saml')(app, config, passport);
app.use('/show', require('./routes/showRouter'));

app.use(function(req, res) {
	res.setHeader("Content-Type", "text/html; charset=utf-8");
	res.status(404).send('Page cannot be found!');
});

// Views
app.engine('ejs', require('ejs-locals'));
app.set('views', config.app.viewsdir);
app.set('view engine', 'ejs');
app.renderFile = (fileName, values) => ejs.render(fs.readFileSync(config.app.viewsdir+ '/'+ fileName+'.ejs', 'UTF8'), values);
express.response.layout = require('./views/layout');

module.exports = app;
