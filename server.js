/**
 * Using: start the server with "npm run watch" command
 *
 */
'use strict';
// Config
const env = process.env.NODE_ENV || 'development';
const config = require('./config/config')[env];
const app = require('./app');
const bodyParser = require('body-parser');

app.use(bodyParser.json({limit: '2mb'}));
app.use(bodyParser.urlencoded({limit: '2mb', extended: true}));

const server = app.listen(config.app.port, () => {
	console.log(`Express is running on port ${server.address().port}`);
});
