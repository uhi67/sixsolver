const path = require('path');

module.exports = {
	development: {
		app: {
			name: 'Passport SAML strategy example',
			port: process.env.PORT || 8080,
			viewsdir: path.dirname(__dirname) + '/views',
			publicdir: path.dirname(__dirname) + '/public',
			logformat: ':remote-addr :req[x-appengine-user-ip] :remote-user :method :url HTTP/:http-version :status :res[content-length] - :response-time ms',
		},
		session: {
			secret: process.env.SESSION_SECRET || 'opweirzn2ct823tzxk3r4itgfc54,cp3',
		},
		passport: {
			strategy: 'saml',
			saml: {
				entryPoint: process.env.SAML_ENTRY_POINT || 'http://localhost/simplesaml/saml2/idp/SSOService.php', // IdP címe
				slo: process.env.SAML_SLO || 'http://localhost/simplesaml/saml2/idp/SingleLogoutService.php', // IdP kilépés
				path: strip_path(process.env.SAML_AssertionConsumerService) || '/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp',  // AssertionConsumerService path része
				logout: strip_path(process.env.SAML_SingleLogoutService) || '/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp',  // AssertionConsumerService path része
				issuer: process.env.SAML_ENTITY ||'sixsolver-local', // SP entity ID
				cert: process.env.SAML_CERT,
				metadata: path.resolve(process.env.SAML_METADATA || 'metadata'),
			}
		}
	}
};

function strip_path(path) {
	if(typeof path !== 'string') return undefined;
	if(path[0]==='*') return path.substr(1);
	return path;
}
