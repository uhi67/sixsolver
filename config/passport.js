const path = require('path');
const fs = require('fs');
if(!Object.entries) require('object.entries').shim();
if(!Object.fromEntries) require('object.fromentries').shim();

const SamlStrategy = require('passport-saml').Strategy;

module.exports = function (passport, config) {
	if(typeof config.passport === 'undefined') console.log('Configuration error: config.passport is undefined');
	console.log(config.passport.saml);

	passport.serializeUser(function (user, done) {
		done(null, user);
	});

	passport.deserializeUser(function (user, done) {
		done(null, user);
	});

	let metadata = config.passport.saml.metadata;
	const attributeMap = metadata && fs.existsSync(attributeMapFile = path.resolve(metadata, 'attributemap/oid2name.json')) && JSON.parse(fs.readFileSync(attributeMapFile, 'utf8'));

	passport.use(new SamlStrategy({
			path: config.passport.saml.path,
			entryPoint: config.passport.saml.entryPoint,
			issuer: config.passport.saml.issuer,
			cert: config.passport.saml.cert,
		},
		function (profile, done) {
			return done(null, {
				profile: profile,
				attributes: Object.fromEntries(Object.entries(profile).map(([key, value]) => [attributeMap[key] || key, value])),
			});
		})
	);

};
