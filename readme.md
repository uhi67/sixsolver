Six Solver
==========
A formula puzzle solver written in node.js
With integrated SAML login. 

Prerequisites
-------------
- node.js + npm (https://nodejs.org)

*Yarn* may be used instead of *npm*, but not recommended to use both.
No apache or other webserver is necessary.

Installation
------------
    > git clone six (or download source code)
    > npm install

Configuration
-------------
- Set port number in server.js
- Configure reverse proxy for the port (optional)

Set environment variables (in deocker-compose or .bashrc)

    SESSION_SECRET=...
    # IdP entry point
    SAML_ENTRY_POINT=http://.../simplesaml/saml2/idp/SSOService.php
    # SLogout service of IdP
    SAML_SLO=http://.../simplesaml/saml2/idp/SingleLogoutService.php
    # Entity ID of SP
    SAML_ENTITY=...
    # path part of SP's AssertionConsumerService (in .bashrc you may have to prepend a * to avoid preprocessing)
    SAML_AssertionConsumerService=/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp
    # path part of SP's AssertionConsumerService (in .bashrc you may have to prepend a * to avoid preprocessing)
    SAML_SingleLogoutService=/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp
    # certificate of the IdP 
    SAML_CERT=MII...=


Run
---

### In development environment: 

    > npm run watch
    
And in browser navigate to: [http://localhost/8080](http://localhost/8080)
    
### In Docker

#### Building the image

In the root directory of the project:

    > docker build -t <your username>/sixsolver-app .

#### Run the image on a specified port

    > docker run -p <external-port>:8080 -d <your username>/sixsolver-app

#### Other docker-related stuff

To get into the running container by command line:

    > docker exec -it <container id> /bin/bash

### In Docker Swarm

Use docker-compose.yml to build the stack.

Testing
--------

For development-time unit testing, first install tzhe testing tool globally in your environment:

    > npm install --global jest
    
Place tests into test directory, and run them:
 
    > jest
