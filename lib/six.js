const tag = require('html-tag');
const format = require('string-format');
Number.isNaN = require('is-nan');
format.extend(String.prototype, {});
Array.prototype.equals = function(aa) {
	if (this === aa) return true;
	if (aa === null) return false;
	if (this.length !== aa.length) return false;
	for (let i=0; i<this.length; ++i) if (this[i] !== aa[i]) return false;
	return true;
};
/**
 * @callback evalCallback
 * @param {int[]|object} bindings
 */

/**
 * @member {string} opName
 * @member {number} operands -- minimal number of operands required for the function
 * @member {number} prio -- priority order (only for display or omit paranthesis)
 * @member {string} display -- string containing $1 placeholders for operands
 * @member {evalCallback} evaluate -- must consume bindings values
 */
class Fn {
	// [operator, params, prio, display, evaluate, nopar, nosubpar, byref]
	constructor(opName, operands=0, prio=0, display='', evaluate='', nopar=false, nosubpar=false, byref=false) {
		this.operands = operands;
		this.prio = prio;
		this.display = display;
		this.evaluate = evaluate;
		this.nopar = nopar;	// No paranthesis on this
		this.nosubpar = nosubpar; // No paranthesis on subexpressions (may be array)
		this.byref = byref; // pass arguments by reference (as expression object) rather than evaluated value (may be array)
		if(Array.isArray(opName)) {
			[this.opName, this.operands, this.prio, this.display, this.evaluate] = opName;
			if(opName.length > 5) this.nopar = opName[5];
			if(opName.length > 6) this.nosubpar = opName[6];
			if(opName.length > 7) this.byref = opName[7];
		}
		else {
			this.opName = opName;
		}
	}
	/**
	 * Operátor humán megjelenítése
	 */
	toString(params) {
		if(typeof this.display === 'function') return this.display(params);
		if(typeof this.display === 'string') return this.display.replace(/\$(\d+)/g, (m,p) => params[p-1]);
		if(this.operands === 0) return '<mi>'+this.opName+'</mi>';
		if(this.operands === 1) return '<mi>'+this.opName+'</mi>'+params[0];
		if(this.operands === 2) return params.join('<mo>'+this.opName+'</mo>');
	}

	static constant(value) {
		return new Fn(value, 0, 0, tag('mn', value.toString()), () => value);
	}

	/**
	 * Creates a variable with name (may contain $)
	 * @param varName
	 * @returns {Fn}
	 */
	static variable(varName) {
		if(typeof varName !== 'string') throw "Variable name must be a string";
		return new Fn(varName, 0, 0, '<mi>' + varName + '</mi>', b => {
			if(b[varName]===undefined) b[varName] = varName;
			return b[varName];
		});
	}

	/**
	 *
	 * @param {number} n
	 * @returns {number}
	 */
	static fakt(n) {
		if(typeof n !== 'number') return NaN;
		if(!Number.isInteger(n)) return NaN;
		if(n<1) return NaN;
		if(n<3) return n;
		if(n>100) return Infinity;
		let p = 2;
		for(let i=3; i<=n; i++) p = p * i;
		return p;
	}

	static mod(a,b) {
		if(typeof a !== 'number' || typeof b !== 'number' || !Number.isInteger(a) || !Number.isInteger(b)) return NaN;
		return a % b;
	}

	static randomInt(max) {
		return Math.floor(Math.random() * Math.floor(max));
	}

}

/**
 * @class Expression
 * @member {Fn} fn
 * @member {Expression[]} ops
 */
class Expression {
	constructor(fn, ops=[]) {
		this.fn = fn;
		this.ops = ops;
	}

	toString() {
		return '{fn}{ops}'.format({
			fn: this.fn.opName,
			ops: Array.isArray(this.ops) && this.ops.length>0 ? '('+this.ops.map(e=>e.toString()).join(',')+')' : ''
		});
	}

	/**
	 * Creates an expression recursively from exp array.
	 * Format: [opname, ...operand]
	 * @param {Map} fns
	 * @param {Array} exp
	 */
	static create(fns, exp) {
		if(!(fns instanceof Map)) throw new Error("Parameter 0 should be a Map");
		if(exp instanceof Expression) return exp;
		if(!Array.isArray(exp)) exp = [exp];
		let opName = exp.shift();
		let fn = null;
		if(typeof opName === 'number') return Expression.constant(opName);
		else {
			if (typeof opName !== 'string') console.log(opName);
			fn = fns.get(opName);
			if (fn === undefined && /^[\w$]+$/.test(opName[0])) fn = Fn.variable(opName);
		}
		return new Expression(fn, exp.map(a => Expression.create(fns, a)));
	}

	static constant(value) {
		return new Expression(Fn.constant(value));
	}
	static variable(name) {
		return new Expression(Fn.variable(name));
	}

	/**
	 * Kifejezés humán megjelenítése
	 * Az alkifejezéseket zárójelbe teszi, ha prioritásuk nagyobb, mint ezé és nincs tiltva
	 */
	mathml() {
		if(!Array.isArray(this.ops)) {
			throw "ops must be an array in Expression";
		}
		return this.fn.toString(this.ops.map((e,i) => {
			let nosubpar = this.fn.nosubpar === undefined ? false : (typeof this.fn.nosubpar === 'boolean' ? this.fn.nosubpar : this.fn.nosubpar[i]);
			let result = e.mathml();
			if(e.fn === undefined) console.log(this, e);
			if(this.fn === undefined) console.log(this);
			if(e.fn.prio > this.fn.prio && !nosubpar && !e.fn.nopar)
				result = '<mo>(</mo>' + result + '<mo>)</mo>';
			if(e.fn.operands > 0 && !e.fn.nopar) result = tag('mrow', result);
			return result;
		}));
	}

	math() {
		return tag('math', {xmlns: "http://www.w3.org/1998/Math/MathML"}, this.mathml());
	}

	/**
	 * Kifejezés értékének kiszámítása a megadott név szerinti behelyettesítésekkel.
	 *
	 * @param {Object} bindings
	 * @return {number}
	 */
	evaluate(bindings) {
		if(typeof this.value !== 'undefined') return this.value;
		if(!this.fn) throw new Error("Expression has no function");
		if(typeof this.fn.evaluate !== 'function') throw new Error("Function "+this.fn.opName+" has no evaluator");
		if(typeof bindings !== "object") throw new Error("Binding must be an object at Expression "+this.fn.opName);
		//console.log(this.fn.opName, bindings);
		let result = null;
		if(this.fn.operands === 0) result = this.fn.evaluate(bindings);
		else {
			let values = this.ops.map((e, i) => (this.fn.byref === true || this.fn.byref[i]) ? e : e.evaluate(bindings));
			result = this.fn.evaluate(values, bindings);
		}
		//console.log('->', this.fn.opName, bindings);
		return this.value = result;
	}

	/**
	 * A kifejezés egyenértékűségét az alkalmazott operátorfa alapján dönti el.
	 * Az operátor akkor egyenértékű, ha a neve és argumentumszáma azonos.
	 * @param expression
	 */
	isSame(expression) {
		if(this.fn.opName !== expression.fn.opName) return false; // Neve eltér
		if(this.fn.operands !== expression.fn.operands) return false; // Névleges argumentumszáma eltér
		if(this.ops.length !== expression.ops.length) return false; // Tényleges operandusainak száma eltér
		for(let i=0; i<this.ops.length; i++) {
			if(!this.ops[i].isSame(expression.ops[i])) return false;
		}
		return true;
	}
}

class Problem {
	constructor(bindings, result) {
		this.solution = null;
		this.bindings = bindings;
		this.result = result;
	}
}

/**
 * @callback heurCallback
 * @param {State} state
 */


/**
 * @class State
 * @member {Expression[]} explist
 * @member {State} parent
 * @member {int} cost -- az aggregált költség
 * @member {int} h
 * @member {int} f
 * @member {heurCallback} h -- heurisztikus függvény
 */
class State {
	/**
	 * @param {Expression[]} explist
	 * @param {State} parent
	 * @param {heurCallback} heur
	 * @param {int} cost -- a lépés elemi költsége
	 */
	constructor(explist, parent = null, heur=null, cost=1) {
		/** @member explist {array} */
		this.explist = explist;
		this.parent = parent;
		this.cost = parent ? parent.cost + cost : 0;
		this.h = typeof heur === 'function' ? heur(this) : 0;
		this.f = this.h + this.cost;
	}

	/**
	 * Az állapot egyenértékűségét a részkifejezések egyezősége alapján dönti el.
	 * @param {State} state
	 * @returns {boolean}
	 */
	isSame(state) {
		if(state.explist.length !== this.explist.length) return false;
		for(let i=0;i<this.explist.length; i++) {
			if(!this.explist[i].isSame(state.explist[i])) return false;
		}
		return true;
	}

	/**
	 * Eldönti, hogy ez az állapot megoldása-e a megadott problémáak
	 * @param {Problem} problem
	 * @returns {boolean}
	 */
	isSolutionOf(problem) {
		if(!Array.isArray(this.explist)) return false;
		if(this.explist.length > 1) return false;
		if(this.explist.length === 0) throw new Error("Zero length expression list");
		return this.explist[0].evaluate(problem.bindings) === problem.result;
	}

	toString() {
		return 'S<[{explist}]:{cost}{parent}>'.format({
			explist: this.explist.map(e=>e.toString()).join(','),
			cost: this.cost,
			parent: this.parent ? '->['+this.parent.explist.map(e=>e.toString()).join(',')+']' : ''
		});
	}

	math() {
		return this.explist.map(e => tag('div', e.math())).join();
	}

	/**
	 * Returns array of evaluated values
	 * @param {number[]} bindings
	 * @returns {number[]}
	 */
	evaluate(bindings) {
		return this.explist.map(e => e.evaluate(bindings));
	}
}

/**
 * Állapotok készlete
 */
class StateSet extends Array {
	constructor(...items) {
		super(...items);
	}

	/**
	 * Megállapítja, hogy az állapot benne van-e a készletben
	 *
	 * @param {State} state
	 * @returns {State|undefined}
	 */
	contains(state) {
		return this.find(s => s.isSame(state)) !== undefined;
	}

	/**
	 * Removes all equivalent occurrences of given State
	 * @param {State} state
	 */
	remove(state) {
		let index;
		while((index = this.findIndex(s => s.isSame(state))) !== -1) {
			this.splice(index, 1);
		}
	}

	toString() {
		return '['+this.map(s=>s.toString()).join('; ')+']';
	}
}

/**
 * @member {Problem} problem
 * @member {StateSet} open
 * @member {StateSet} closed
 * @member {Map} fns
 * @member {int} maxCost
 * @member {heurCallback} heur -- heurisztikus függvény
 * @member {int} limit -- kiterjesztések számának korlátozása
 */
class Database {
	constructor(problem, fns, maxCost, heur=null, limit=2000) {
		this.open = new StateSet();
		this.closed = new StateSet();
		this.problem = problem;
		this.fns = fns;
		this.maxCost = maxCost;
		this.heur = heur;
		this.limit = limit;
	}

	/**
	 * Létrehozza az állapot leszármazottait a nyílt halmazban és átteszi az állapotot a zártba
	 * @param {State} state
	 */
	extend(state) {
		for(let i=0; i<state.explist.length; i++) { // Minden részkifejezésen megpróbáljuk indítani
			for (let [op, fn] of this.fns.entries()) {
				// op függvény az i-ik részkifejezésen, ha lehet.
				if(i + fn.operands > state.explist.length) continue; // Nincs elég kifejezés a függvényhez
				let newExplist = state.explist.slice();
				newExplist.splice(i, fn.operands, new Expression(fn, state.explist.slice(i, i+fn.operands)));
				let newState = new State(newExplist, state, this.heur, 1);
				// Ha bármelyik részkifejezés NaN, akkor eldobjuk.
				let valid = newState.explist.reduce((v, e) => v && !Number.isNaN(e.evaluate(this.problem.bindings)), true);
				// Ha értékei megegyeznek a szülőével, eldobjuk
				valid = valid && !newState.evaluate(this.problem.bindings).equals(state.evaluate(this.problem.bindings));
				if(valid && newState.cost <= this.maxCost && !this.open.contains(newState) && !this.closed.contains(newState)) {
					newState.parent = state;
					this.open.push(newState);
					// Megnézzük, hogy az új csomópont megoldása-e a problémának, vagy esetleg jobb megoldás-e.
					if((!this.problem.solution || this.problem.solution.cost > newState.cost) && newState.isSolutionOf(this.problem)) {
						this.problem.solution = newState;
					}
				}
			}
		}
		this.closed.push(state);
		this.open.remove(state);
		return true;
	}

	/**
	 * Egy kezdőállapottal indulva addig keres, amíg az összes problémára megoldást nem talál, vagy kifogy a korlátozott gráfból
	 *
	 * @param {State} startState
	 * @returns {boolean} -- megtalált minden megoldást
	 */
	search(startState, all=false) {
		this.open.push(startState);
		let ready = false;
		while(this.open.length !== 0 && this.closed.length < this.limit) {
			let state = this.open.reduce((best, state) => state.f < best.f || (state.f === best.f && Math.random()<0.5) ? state : best);
			//let state = this.open[Fn.randomInt(this.open.length)]; // random verzió
			if(this.extend(state)) {
				// Sikeres kiterjesztés. Megoldás?
				ready = this.problem.solution !== null;
				if(!all && ready) break; // Első megoldásnál terminál
			}
		}
		return ready;
	}

}

const fns = new Map([
	// operator, params, prio, display, evaluate, nopar, nosubpar, byref
	['neg', 1, 2, '<mo>(</mo><mo>-</mo>$1<mo>)</mo>', b => -b.shift()],
	['fakt', 1, 1, '$1<mo>!</mo>', b => Fn.fakt(b.shift())],
	// ['sin', 1, 2, null, b => Math.sin(b.shift())],
	// ['cos', 1, 2, null, b => Math.cos(b.shift())],
	// ['sgn', 1, 2, null, x => x<0 ? -1 : (x>0 ? 1 : 0)],
	['sqrt', 1, 2, '<msqrt>$1</msqrt>', b => Math.sqrt(b.shift()), true, true],
	['root', 2, 2, '<mroot>$2$1</mroot>', b => {let r = b.shift(); return Math.pow(b.shift(), (1 / r));}, true, true],		// n-ik gyök
	['exp', 1, 2, '<msup><mi>e</mi><mn>$1</mn></msup>', b => Math.exp(b.shift())],
	['ln', 1, 2, null, b => Math.log(b.shift())],
	['log', 2, 2, '<msub><mi>log</mi>$1</msub> $2', b => { let base = b.shift(); return Math.log(b.shift()) / Math.log(base); }, true, [true, false]],
	// ['lg', 1, 2, '<mi>lg</mi>$1', b => Math.log(b.shift()) / Math.log(10)],
	//['ex', 1, 1, '<msup><mi>e</mi><mn>$1</mn></msup>', b => b.shift()===0 ? 1 : NaN],
	['^', 2, 4, '<msup>$1<mn>$2</mn></msup>', b => Math.pow(b.shift(), b.shift())],
	['*', 2, 5, null, b => b.reduce((a, v) => a * v, b.shift())],
	['/', 2, 5, '<mfrac>$1$2</mfrac>', b => b.reduce((a, v) => a / v, b.shift()), true, true],
	['%', 2, 5, '$1 <mo>%</mo> $2', b => Fn.mod(b.shift(), b.shift())],
	['+', 2, 6, null, b => b.reduce((a, v) => a + v, b.shift())],
	['-', 2, 6, null, b => b.reduce((a, v) => a - v, b.shift())],
].map(def => [def[0], new Fn(def)]));


const fnLet = new Fn(':=', 2, 10, null, (v,b) => { return (b[v[0]] = v[1]);});
const fnSum = new Fn('Sum', 4, 2, '<munderover><mo>&Sum;</mo><mrow>$1<mo>=</mo>$2</mrow>$3</munderover>$4', (v,b) => {
	//console.log(v);
	if(!(v[3] instanceof Expression)) throw "Sum expects parameter 4 by reference";
	let varName = v[0];
	let sum = 0;
	for(let i=v[1]; i<=v[2]; i++) {
		b[varName] = i;
		sum += v[3].evaluate(b);
	}
	return sum;
}, false, false, [false, false, false, true]);

module.exports = {Fn, Expression, Problem, State, StateSet, Database, fns, fnLet, fnSum};
