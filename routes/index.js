const requestIp = require('request-ip');

module.exports = function (app) {
	app.get('/', (req, res) => {
		console.log('/', requestIp.getClientIp(req));
		res.layout(req, 'index', {});
	});
};
