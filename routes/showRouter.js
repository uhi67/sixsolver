const express = require('express');
const router = express.Router();

// Require controller modules.
const showController = require('./showController');

router.get('/', showController.default);
router.get('/rand', showController.rand);
router.get('/solve', showController.solve);
router.get('/all', showController.all);

module.exports = router;
