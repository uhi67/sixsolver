module.exports = function (app) {
	app.all('*', (req, res, next) => {
		req.session.mainMenu = require('./mainMenu').slice(); // Clone to avoid storing runtime changes
		if (req.isAuthenticated()) {
			req.session.mainMenu[2] = {
				active: true,
				class: 'user',
				link: '/profile',
				caption: req.user.attributes.displayName,
				icon: 'fa-user'
			};
			req.session.mainMenu[3] = {active: true, link: '/logout', caption: 'Logout', icon: 'fa-sign-out-alt'};
		}
		next();
	});
};
