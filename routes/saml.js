const requestIp = require('request-ip');

module.exports = function (app, config, passport) {
	app.get('/login', passport.authenticate(config.passport.strategy, {
			successRedirect: '/',
			failureRedirect: '/failure'
	}));

	app.post(config.passport.saml.path,
		passport.authenticate(config.passport.strategy, {
			failureRedirect: '/failure',
			failureFlash: true
		}),
		function (req, res) {
			console.log('saml-path', requestIp.getClientIp(req));
			res.redirect('/');
		}
	);

	app.get('/failure', (req, res) => {
		console.log('login failed');
		res.layout(req, 'login failed');
	});

	app.get([config.passport.saml.logout, '/logout'], (req, res) => {
		req.logout();
		console.log('logout:', req.isAuthenticated());
		req.session.destroy(err => {
			console.log('Session destroyed:', err);
			// TODO: goto config.passport.saml.slo with proper query
			res.redirect('/');
		});
	});

	app.get('/profile', (req, res) => {
		if (req.isAuthenticated()) {
			res.layout(req, 'profile', {
				user: req.user
			});
		} else {
			res.redirect('/login');
		}
	});
};

