module.exports = [
	{ active: true,link: '/', caption: 'Home', icon: 'fa-home'},
	{ active: true,link: '/show/solve', caption: 'Solve', icon: 'fa-square-root-alt'},
	{ active: true,link: '/login', caption: 'Login', class: 'samlbutton eduid-login'},
];
