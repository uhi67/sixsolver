const tag = require('html-tag');
const six = require('../lib/six.js');
const fns = six.fns;
const Expression = six.Expression;
const State = six.State;
const Problem = six.Problem;
const Database = six.Database;

class ShowController {
	static default(req, res) {
		console.log('show/default');
		let fns1 = new Map(fns);
		fns1.set(',', new six.Fn(',', -2, 99, null, v => v));
		fns1.set(':=', six.fnLet);
		fns1.set('=', new six.Fn('=', 2, 9, null, v => v.shift() === v.shift()));
		fns1.set('sum', six.fnSum);

		let exp0 = Expression.create(fns1, [',',
			[':=', '$a', 2],
			[':=', '$b', -5],
			[':=', '$c', 2],
		]);
		let exp1 = Expression.create(fns, ['/',
			['-', ['neg', ['$b']], ['sqrt', ['-', ['^', '$b', 2], ['*', 4, '$a', '$c']]]],
			['*', 2, ['$a']]
		]);

		let b = {};
		let v = exp0.evaluate(b);
		console.log(b, v);

		let r = Expression.constant(exp1.evaluate(b));
		console.log(b);
		let exp1a = Expression.create(fns1, ['=', exp1, r]);
		console.log(exp1a.ops[1]);

		let exp2 = Expression.create(fns1, [
			'sum', 'n', 1, 5, ['^', 'n', 2]
		]);
		res.render('layout.ejs', {
			content:
				tag('div', exp0.math()) +
				tag('div', exp1a.math()) +
				tag('div', exp2.math())
		});
	}

	static rand(req, res) {
		console.log('show/rand');
		res.layout(req, 'Show rand');
	}

	static solve(req, res) {
		console.log('Query:', req.query);
		let all = false; //!!req.query.all;
		let maxCost = 7; // Legnagyobb költség (operátorok maximális száma)
		let limit = 4000;
		let r = req.query.r ? parseInt(req.query.r) : 6;
		let v = req.query.v ? req.query.v.map(v=>parseInt(v)) : [1,2,3];
		let vars = {}; v.forEach((v,i)=>{vars[String.fromCharCode(i+'a'.charCodeAt(0))] = v; });
		let problem = new Problem(vars, r);
		let heur0 = s => s.explist.length/2;
		let heur1 = s => Math.abs(s.explist.reduce((a, e) => a + e.evaluate(problem.bindings), 0) - problem.result);
		let heur2 = s => Math.abs(s.explist.reduce((a, e) => a * e.evaluate(problem.bindings), 1) - problem.result);
		let heur3 = s => s.explist.reduce((a, e) => a = a + (e.evaluate(problem.bindings)===0 ? 1 :0), 0);
		let heur = s => heur0(s) + Math.min(heur1(s), heur2(s));
		let database = new Database(problem, fns, maxCost, heur, limit);
		let startState = new State(Object.keys(problem.bindings).map(v => Expression.create(fns, v)));
		let result = database.search(startState, all);
		let rrr = result ?
			(all ?
				database.closed.filter(s=>s.isSolutionOf(problem)).map(s=>tag('div', s.math())).join('') :
				problem.solution.math()
			) :
			"No solution";
		let vvv = v.map((v,i) => req.app.renderFile('solve_input', {
			i: i,
			v: v,
			vn: Object.keys(vars)[i]
		})).join('');

		res.layout(req, 'solve', {
			vvv: vvv,
			r: r,
			result: rrr,
			closedNodes: database.closed.length,
			openNodes: database.open.length,
			limit: database.limit,
			fnsx: fns.size,
			fnsl: Array.from(fns, ([key, value]) => value).map(f=>f.toString(['a', 'b'].map(a=>'<mi>'+a+'</mi>'))).join('<mo>,</mo> '),
			cost: result ? problem.solution.cost : '',
			maxCost: database.maxCost
		});
	}

	static all(req, res) {
		for (let i = 0; i < 10; i++) {
			problem = new Problem({a:i, b:i, c:i}, 6);
			let database = new Database(problem, fns, 4);
			let result = database.search(new State(problem.bindings.keys().map(v => Expression.create('$'+v))));
			console.log(i, result);
		}

		res.render('layout.ejs', {
			content:
				tag('div', exp0.math())
		});
	}
}
module.exports = ShowController;
