/**
 * Usage
 *
 * In server.js:
 *
 * 		express.response.layout = require('./views/layout');
 *
 * In controller:
 *
 * 		res.layout(req, object); // full data for layout, including content
 * 		res.layout(req, string, object, object);	// render a view for content with optional layoutdata
 * 		res.layout(req, string); // text content only
 *
 *
 * @param {Request} req
 * @param {object|string} data|content|view
 * @param {object|undefined} viewdata
 * @param {object|undefined} layoutdata
 */
module.exports = function(req, view, viewdata, layoutdata) {
	let mainMenu = this.app.renderFile('mainMenu', {data: req.session.mainMenu});
	let data = {
		mainMenu: mainMenu,
	};
	if(typeof layoutdata === 'object') data = Object.assign(data, layoutdata);

	if(typeof view === 'string' && typeof viewdata==='undefined') {
		data.content = view;
	}

	if(typeof view === 'string' && typeof viewdata==='object') {
		data.content = this.app.renderFile(view, viewdata);
	}

	if(typeof view === 'object') {
		data = Object.assign(data, view);
	}

	this.render('layout.ejs', data);
};
